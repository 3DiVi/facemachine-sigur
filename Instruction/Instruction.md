## Инструкция по использованию интеграции Sigur & Face Machine

* 1.) Редактировать файл конфигурации Face Machine.
    - a.) Перейти в папку config:
       - Путь до папки config:
          - Linux: /home/<user>/.local/share/FaceMachineClient/config
          - Windows: C:\Users\ <user>\AppData\Local\FAceMachineClient\config
    - b) Открыть файл run_params.json.
    - c) В файле run_params.json найти: 
        ```json            
                    "__comment_webhook_tracking_subscribers": [
                         {
                            "host": "127.0.0.1",
                            "port": "5000",
                            "path": "/tracking"
                        }
                    ]
        ```            
        И заменить это на 
        ```json
            "webhook_tracking_subscribers": [
                             {
                                "host": "127.0.0.1",
                                "port": "9000",
                                "path": "/event_hook"
                            }
                        ]
        ```
        
    - d.) Сохранить изменения.
    
    
* 2.) Создать Edge device с помощью API вызова (см. документацию - https://docs.facemachine.3divi.com/?version=3002fe83-8c78-4dda-8bc4-09658ad0256d#7a1d4310-7530-41ee-b905-d5b458cc67db) либо в разделе "Edge devices" веб-интерфейса Face Machine Server.
* 3.) Запустить приложение Face Machine Client (FMC), перейти в веб-конфигуратор (см. документацию - https://docs.facemachine.3divi.com/?version=3002fe83-8c78-4dda-8bc4-09658ad0256d#getting-started) и активировать токен созданного на предыдущем шаге Edge device. 
* 4.) Запустить сервис интеграции (Docker образ):
      
      
       docker run --rm -d --name sigur -e "SERVICE_PORT=9000" -e "APP_WORKERS=4" -e "TOKEN=API token FMC" -e "FACEMACHINE_URL=https://facemachine.3divi.com" -p 9000:9000 registry.gitlab.com/3divi/facemachine-sigur:latest
       
      
      
     Анонимный режим, переменная для активации `"ANONYMOUS_MODE=True"` , при активации этого режима все персоны в FMC будут создаваться с скрытым шаблоном.
     
      
      docker run --rm -d --name sigur -e "SERVICE_PORT=9000" -e "APP_WORKERS=4" -e "TOKEN=API token FMC" -e "FACEMACHINE_URL=https://facemachine.3divi.com" -e "ANONYMOUS_MODE=True" -p 9000:9000 registry.gitlab.com/3divi/facemachine-sigur:latest
      
      

* 5.) Открыть клиент Sigur.
* 6.) Перейти в раздел настроек "Видеонаблюдение:

        * Путь до Видеонаблюдение: Файл/Настройки/Видеонаблюдение

* 7.) В появившемся диалоговм окне выставить настройки:

        - Имя Сервера: <произвольное>
        
        - Тип сервера: Пользовательская система
        
        - Адрес сервера: 127.0.0.1
        
        - Порт сервера: 9000
        
        - Путь к сервису: /
        
        - Имя пользователя: оставить поле пустым
        
        - Пароль: оставить поле пустым
        
        - Аутенфикация: выбираем "HTTP Аутенфикация"
        
        - Поставить галки на пунктах "Выгружать на сервер сотрудников и фотографии" и "Получать с сервера события"
        
![](Screenshot%20from%202020-02-07%2012-08-39.png)

* 8.) Перейти в раздел настроек "Распознавание лиц" и активировать функцию "Распознавание лиц". 

![](Screenshot%20from%202020-02-07%2012-08-57.png)

* 9.) Перейти в основное меню и выбрать раздел "Оборудование", найти в этом разделе вкладку "Видеонаблюдение", назначить канал для направлений входа и выхода этой точки. Далее выбрать пользовательскую систему, затем выбрать настройки камеры, отметить все галочки и нажать "Применить".

![](settings.png)

* 10.) Чтобы убедиться, что события по факту распознавания приходят в систему, необходимо перейти в раздел "Наблюдение" в основном меню.
